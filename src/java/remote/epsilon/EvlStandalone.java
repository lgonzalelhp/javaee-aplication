package remote.epsilon;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.epsilon.eol.IEolModule;
import org.eclipse.epsilon.eol.models.IModel;
import org.eclipse.epsilon.evl.EvlModule;
import org.eclipse.epsilon.evl.execute.UnsatisfiedConstraint;




/**
 * This example demonstrates using the 
 * Epsilon Object Language, the core language
 * of Epsilon, in a stand-alone manner 
 * @author Dimitrios Kolovos
 */
public class EvlStandalone extends EpsilonStandalone {
  
    private String xmi;
    private String ecore;
    private String evl;

    public EvlStandalone (String xmi, String ecore, String evl) {
        this.xmi = xmi;
        this.ecore = ecore;
        this.evl = evl;
    }

    @Override
    public IEolModule createModule() {
            return new EvlModule();
    }

    @Override
    public List<IModel> getModels() throws Exception {
            List<IModel> models = new ArrayList<IModel>();
            models.add(createEmfModel("Model", this.xmi, this.ecore, true, true));
            return models;
    }

    @Override
    public String getSource() throws Exception {
            return this.evl;
    }

    @Override
    public String  postProcess() {
            EvlModule module = (EvlModule) this.module;
            String result = "";
            Collection<UnsatisfiedConstraint> unsatisfied = module.getContext().getUnsatisfiedConstraints();

            if (unsatisfied.size() > 0) {
                    result+= unsatisfied.size() + " constraint(s) have not been satisfied<br>";
                    for (UnsatisfiedConstraint uc : unsatisfied) {
                           result+=uc.getMessage()+"<br>";
                    }
            }
            else {
                    result+="All constraints have been satisfied";
            }
            return result;
    }
  
}