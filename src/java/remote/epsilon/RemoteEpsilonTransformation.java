/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package remote.epsilon;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author luis
 */
@MultipartConfig()
@WebServlet(name = "RemoteEpsilonTransformation", urlPatterns = {"/RemoteEpsilonTransformation"})
public class RemoteEpsilonTransformation extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int n;
        n = (int) (Math.random()*100);
        
        String res = "";
        
        response.setContentType("text/html;charset=UTF-8");
        
        Map <String, String[]> params = request.getParameterMap();
        ArrayList <File> files = new ArrayList<>();
        File ecorefile = null;
        File egxfile = null;
        for (Map.Entry<String, String[]> entry : params.entrySet()) {
            if (!entry.getKey().equals("xmi") && !entry.getKey().equals("metamodelname")){
                System.out.println("clave=" + entry.getKey() + ", valor=" + entry.getValue()[0]);
                if(entry.getKey().equals("ecore")){
                    ecorefile = File.createTempFile(entry.getKey(),".ecore");
                    files.add(ecorefile);
                }
                else if(entry.getKey().equals("egx")){
                    egxfile = File.createTempFile(entry.getKey(),".egx");
                    files.add(egxfile);
                }
                else
                    files.add(new File("/tmp/"+entry.getKey()));
                URLConnection conn = new URL(entry.getValue()[0]).openConnection();
                conn.connect();
                InputStream in = conn.getInputStream();
                OutputStream out = new FileOutputStream(files.get(files.size()-1));
                int b = 0;
                while (b != -1) {
                  b = in.read();
                  if (b != -1)
                    out.write(b);
                }
                out.close();
                in.close();
            }
        }
 
   
        Part xmiPart = request.getPart("xmi"); 
        File xmifile = new File("/tmp/", "xmi"+n+".evl");
        InputStream xmiinput = xmiPart.getInputStream();
        Files.copy(xmiinput, xmifile.toPath(), StandardCopyOption.REPLACE_EXISTING);        
                
	EgxStandalone egxS = new EgxStandalone(egxfile.getPath(), xmifile.getPath(), ecorefile.getPath());
        
        try {
        res = egxS.execute();
        Path p = Files.createFile(Paths.get("/tmp/gen.zip"));
        try (ZipOutputStream zs = new ZipOutputStream(Files.newOutputStream(p))) {
            Path pp = Paths.get("/tmp/egx-gen");
            Files.walk(pp)
              .filter(path -> !Files.isDirectory(path))
              .forEach(path -> {
                  ZipEntry zipEntry = new ZipEntry(pp.relativize(path).toString());
                  try {
                      zs.putNextEntry(zipEntry);
                      Files.copy(path, zs);
                      zs.closeEntry();
                } catch (IOException e) {
                    System.err.println(e);
                }
              });
        }
            
        } catch (Exception ex) {
            Logger.getLogger(RemoteEpsilonTransformation.class.getName()).log(Level.SEVERE, null, ex);
        }
	
        response.setContentType("application/zip");
        response.setHeader("Content-disposition", "attachment; filename=gen.zip");
 
        OutputStream out = response.getOutputStream();
        FileInputStream in = new FileInputStream("/tmp/gen.zip");
        byte[] buffer = new byte[4096];
        int length;
        while ((length = in.read(buffer)) > 0){
            out.write(buffer, 0, length);
        }
        in.close();
        out.flush();
        out.close();
        

        for (int i =0; i<files.size(); i++){
            Files.delete(files.get(i).toPath());
        }
        files.clear();
        Files.delete(xmifile.toPath());
        xmifile = null;
        File f = new File ("/tmp/gen.zip");
        Files.delete(f.toPath());
        File directory = new File("/tmp/egx-gen");
        File[] fList = directory.listFiles();
        if(fList != null)
            for (File file : fList) {      
               Files.delete(file.toPath());
            }
  
        File f2 = new File ("/tmp/egx-gen");
        Files.delete(f2.toPath());
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
