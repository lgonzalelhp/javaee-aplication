/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * Contributors:
 *     Dimitrios Kolovos - initial API and implementation
 ******************************************************************************/
package remote.epsilon;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.epsilon.egl.EglFileGeneratingTemplateFactory;
import org.eclipse.epsilon.egl.EgxModule;
import org.eclipse.epsilon.eol.IEolModule;
import org.eclipse.epsilon.eol.models.IModel;

/**
 * This example demonstrates using the 
 * Epsilon Generation Language, the M2T language
 * of Epsilon, in a stand-alone manner 
 * @author Dimitrios Kolovos
 */
public class EgxStandalone extends EpsilonStandalone {
	protected String egx;
        protected String xmi;
        protected String ecore;
        public EgxStandalone (String egx, String xmi, String ecore){
            this.egx = egx;
            this.xmi = xmi;
            this.ecore = ecore;
        }
	
	
	@Override
	public IEolModule createModule() {
		try {
			EglFileGeneratingTemplateFactory templateFactory = new EglFileGeneratingTemplateFactory();
			templateFactory.setOutputRoot(new File("/tmp/egx-gen").getAbsolutePath());
			return new EgxModule(templateFactory);
		}
		catch (Exception ex) { 
			throw new RuntimeException(ex);
		}
	}

	@Override
	public List<IModel> getModels() throws Exception {
		List<IModel> models = new ArrayList<IModel>();
		models.add(createEmfModel("Model", this.xmi, this.ecore, true, false));
		return models;
	}

	@Override
	public String getSource() throws Exception {
		return this.egx;
	}

}
