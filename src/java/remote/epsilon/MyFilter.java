
package remote.epsilon;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MyFilter implements Filter{

    @Override
    public void destroy() {
        // TODO Auto-generated method stub

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
            FilterChain chain) throws IOException, ServletException {
        // Process the request / response here. Add or remove the information you need.
        // Add or edit the headers you want to
        System.out.println("DOING A FILTER");
        HttpServletResponse http = (HttpServletResponse) servletResponse;
        http.addHeader("Access-Control-Allow-Origin", "*");
        http.addHeader("Access-Control-Allow-Credentials", "true");
        http.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
        http.addHeader("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
        
        chain.doFilter(servletRequest, servletResponse);            
    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {


    }
}
