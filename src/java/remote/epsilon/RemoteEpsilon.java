/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package remote.epsilon;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author luis
 */
@MultipartConfig()
@WebServlet(name = "RemoteEpsilon", urlPatterns = {"/RemoteEpsilon"})
public class RemoteEpsilon extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

 /*       
//        InputStream fileContent = ecorePart.getInputStream();
//        StringBuilder textBuilder = new StringBuilder();
//        try (Reader reader = new BufferedReader(new InputStreamReader
//          (fileContent, Charset.forName(StandardCharsets.UTF_8.name())))) {
//            int c = 0;
//            while ((c = reader.read()) != -1) {
//                textBuilder.append((char) c);
//            }
//        }
//        System.out.println(textBuilder.toString());
      */  
        String res = ""; 
        
        Map <String, String[]> params = request.getParameterMap();
        ArrayList <File> files = new ArrayList<>();
        File ecorefile = null;
        File evlfile = null;
        for (Map.Entry<String, String[]> entry : params.entrySet()) {
            if (!entry.getKey().equals("xmi")){
                System.out.println("clave=" + entry.getKey() + ", valor=" + entry.getValue()[0]);
                if(entry.getKey().equals("ecore")){
                    ecorefile =  File.createTempFile(entry.getKey(),".ecore");
                    files.add(ecorefile);
                }
                if(entry.getKey().equals("evl")){
                    evlfile =  File.createTempFile(entry.getKey(),".evl");
                    files.add(evlfile);
                }
                
                URLConnection conn = new URL(entry.getValue()[0]).openConnection();
                conn.connect();
                InputStream in = conn.getInputStream();
                OutputStream out = new FileOutputStream(files.get(files.size()-1));
                int b = 0;
                while (b != -1) {
                  b = in.read();
                  if (b != -1)
                    out.write(b);
                }
                out.close();
                in.close();
            }
        }
 
   
        Part xmiPart = request.getPart("xmi"); 
        File xmifile =  File.createTempFile("xmi",".xmi");
        InputStream xmiinput = xmiPart.getInputStream();
        Files.copy(xmiinput, xmifile.toPath(), StandardCopyOption.REPLACE_EXISTING);        
                
	
        EvlStandalone evlS = new EvlStandalone(xmifile.getPath(), ecorefile.getPath(), evlfile.getPath());
        
        try {
            res = evlS.execute();
        } catch (Exception ex) {
            Logger.getLogger(RemoteEpsilon.class.getName()).log(Level.SEVERE, null, ex);
        }
	try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println(res);
        }
    
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
